package com.active.mq.app.controller;

import com.active.mq.app.model.Employee;
import com.active.mq.app.model.EmployeeRequest;
import com.active.mq.app.sender.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.JmsException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/message")
public class MessageController {

    private Logger LOGGER = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    private MessageSender messageSender;

    @GetMapping(value = "/send/queue")
    public ResponseEntity<String> sendMessageToQueue(@RequestBody String message) throws Exception {
        LOGGER.info("Entry into sendMessageToQueue");
        try {
            messageSender.sendSimpleMessage(message);
            return new ResponseEntity("Message sent successfully...", HttpStatus.OK);
        } catch (Exception ex) {
            throw new Exception("Message send failed due to :: ", ex.getCause());
        }
    }

    @PostMapping(value = "/send/employee/details", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendEmployeeDetails(@RequestBody EmployeeRequest request) throws Exception {
        LOGGER.info("Entry into sendEmployeeDetails");
        try {
            messageSender.sendComplexMessage(request);
        } catch (Exception ex) {
            throw new Exception("Message send failed due to :: ", ex.getCause());
        }
        return new ResponseEntity("Employee Details sent successfully...", HttpStatus.OK);
    }

    @GetMapping(value = "/send/topic")
    public ResponseEntity<String> sendMessageToTopic(@RequestBody String message) throws Exception {
        LOGGER.info("Entry into sendMessageToTopic");
        try {
            messageSender.sendMessageToTopic(message);
            return new ResponseEntity("Message sent successfully...", HttpStatus.OK);
        } catch (Exception ex) {
            throw new Exception("Message send failed due to :: ", ex.getCause());
        }
    }
}
