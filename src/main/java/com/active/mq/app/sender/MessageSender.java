package com.active.mq.app.sender;

import com.active.mq.app.model.Employee;
import com.active.mq.app.model.EmployeeRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageSender {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(MessageSender.class);

    @Value("${destination.queue}")
    private String destinationQueue;

    @Value("${destination.topic}")
    private String destinationTopic;

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendSimpleMessage(String message) {
        LOGGER.debug("Sending message='{}'", message);
        jmsTemplate.convertAndSend(destinationQueue, message);
    }

    public void sendComplexMessage(EmployeeRequest request) {
        LOGGER.debug("Sending message='{}'", request);
        jmsTemplate.convertAndSend(destinationQueue, request);
    }

    public void sendMessageToTopic(String message) {
        LOGGER.debug("Sending message='{}'", message);
        jmsTemplate.convertAndSend(destinationTopic, message);
    }
}
