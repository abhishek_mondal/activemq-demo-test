package com.active.mq.app.receiver;

import com.active.mq.app.model.Employee;
import com.active.mq.app.model.EmployeeRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CountDownLatch;

@Component
public class MessageReceiver {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(MessageReceiver.class);

    private CountDownLatch latch = new CountDownLatch(2);

    public CountDownLatch getLatch() {
        return latch;
    }

    @JmsListener(destination = "${destination.queue}")
    public void receive(Object message) {
        LOGGER.info("Received message='{}'", message);
        latch.countDown();
    }

//    @JmsListener(destination = "${destination.queue}")
//    public void receive1(EmployeeRequest message) {
//        LOGGER.info("Received message='{}'", message);
//        latch.countDown();
//    }

    @JmsListener(destination = "${destination.topic}")
    public void receive1(String message) {
        LOGGER.info("'subscriber1' received message='{}'", message);
        latch.countDown();
    }

    @JmsListener(destination = "${destination.topic}")
    public void receive2(String message) {
        LOGGER.info("'subscriber2' received message='{}'", message);
        latch.countDown();
    }
}
