package com.active.mq.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class EmployeeRequest implements Serializable  {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "employees")
    private List<Employee> employees;
}
