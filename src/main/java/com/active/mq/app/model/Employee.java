package com.active.mq.app.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonPropertyOrder({ "empId", "empName", "designation" })
public class Employee implements Serializable {

    private Long empId;
    private String empName;
    private String designation;

}
